import Adafruit_DHT
import sys
from flask import Flask, Response


sensor = Adafruit_DHT.DHT22
app = Flask(__name__)

pin = None

@app.route('/', methods=['GET'])
def get():
    content = 'dht22_humidity {}\ndht22_temperature {}\n'.format(*Adafruit_DHT.read_retry(sensor, pin))

    return content, 200

def main():
    if len (sys.argv) != 4:
        print('Usage: {} <listen_address> <port> <gpio-pin>'.format(sys.argv[0]))
        sys.exit(1)

    global pin
    pin = int(sys.argv[3])

    app.run(host=sys.argv[1], port=int(sys.argv[2]))
