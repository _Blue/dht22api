# An API endpoint for a DHT22 sensor

This repository contains a basic HTTP endpoint to fetch information from a DHT22 sensor.

## Usage:

Install with: `pip install git+https://bitbucket.org/_Blue/dht22api`

Run with: `dht22api <listen-address> <listen-port> <gpio-port>`

## Sample output

```
curl localhost:9000
dht22_humidity 45.5
dht22_temperature 23.700000762939453
```

## Sample systemd unit

```
# /etc/systemd/system/dht22.service

[Unit]
Description=dht22

[Service]
Type=simple
ExecStart=/usr/local/bin/dht22api 0.0.0.0 9000 22
User=nobody
Group=nogroup

[Install]
WantedBy=multi-user.target
```
