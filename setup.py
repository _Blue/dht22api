from setuptools import setup, find_packages

setup(
    name="dht22api",
    version="1",
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        "console_scripts": ["dht22api = dht22api:main"]
        },
    install_requires=[
        'Flask>=0.12.2',
        'Adafruit-DHT==1.4.0'
        ]
    )

